//! Nettle bindings

#![warn(missing_docs)]

extern crate libc;
extern crate nettle_sys;
extern crate rand;
#[macro_use]
extern crate failure;

mod errors;
pub use crate::errors::{Error, Result};

mod helper;

pub mod hash;
pub use crate::hash::Hash;

pub mod cipher;
pub use crate::cipher::{BlockSizeIs16, Cipher};

pub mod mode;
pub use crate::mode::Mode;

pub mod aead;
pub use crate::aead::Aead;

pub mod mac;
pub use crate::mac::Mac;

pub mod kdf;
pub mod rsa;

pub mod random;
pub use crate::random::{Random, Yarrow};

pub mod ecc;
pub use crate::ecc::{
    Curve, Point, Scalar, Secp192r1, Secp224r1, Secp256r1, Secp384r1, Secp521r1,
};

pub mod curve25519;
pub mod dsa;
pub mod ecdh;
pub mod ecdsa;
pub mod ed25519;
