//! Authenticated encryption mode with associated data.

mod aead;
pub use self::aead::Aead;

mod eax;
pub use self::eax::Eax;
mod gcm;
pub use self::gcm::Gcm;
mod ccm;
pub use self::ccm::Ccm;
mod chacha_poly1305;
pub use self::chacha_poly1305::ChaChaPoly1305;
