use nettle_sys::{
    aes128_ctx, nettle_aes128_decrypt, nettle_aes128_encrypt,
    nettle_aes128_invert_key, nettle_aes128_set_decrypt_key,
    nettle_aes128_set_encrypt_key,
};
use std::cmp::min;
use std::mem::zeroed;
use std::os::raw::c_void;

use crate::cipher::RawCipherFunctionPointer;
use crate::{Cipher, Error, Result};

/// 128 bit variant of the Advanced Encryption Standard (AES, formerly RIJNDAEL) defined in FIPS 197.
pub struct Aes128 {
    context: aes128_ctx,
}

impl Aes128 {
    /// Creates a new `Aes128` instance for decryption that uses the same key as `encrypt`. The
    /// `encrypt` instance must be configured for encryption. This is faster than calling
    /// `with_decrypt_key`.
    pub fn with_inverted_key(encrypt: &Self) -> Self {
        let mut ctx = unsafe { zeroed() };
        unsafe {
            nettle_aes128_invert_key(
                &mut ctx as *mut _,
                &encrypt.context as *const _,
            );
        }

        Aes128 { context: ctx }
    }
}

impl Cipher for Aes128 {
    const BLOCK_SIZE: usize = ::nettle_sys::AES_BLOCK_SIZE as usize;
    const KEY_SIZE: usize = ::nettle_sys::AES128_KEY_SIZE as usize;

    fn with_encrypt_key(key: &[u8]) -> Result<Aes128> {
        if key.len() != Aes128::KEY_SIZE {
            return Err(Error::InvalidArgument { argument_name: "key" }.into());
        }
        let mut ctx = unsafe { zeroed() };
        unsafe {
            nettle_aes128_set_encrypt_key(&mut ctx as *mut _, key.as_ptr());
        }

        Ok(Aes128 { context: ctx })
    }

    fn with_decrypt_key(key: &[u8]) -> Result<Aes128> {
        if key.len() != Aes128::KEY_SIZE {
            return Err(Error::InvalidArgument { argument_name: "key" }.into());
        }
        let mut ctx = unsafe { zeroed() };
        unsafe {
            nettle_aes128_set_decrypt_key(&mut ctx as *mut _, key.as_ptr());
        }

        Ok(Aes128 { context: ctx })
    }

    fn encrypt(&mut self, dst: &mut [u8], src: &[u8]) {
        unsafe {
            nettle_aes128_encrypt(
                &mut self.context as *mut _,
                min(src.len(), dst.len()),
                dst.as_mut_ptr(),
                src.as_ptr(),
            )
        };
    }

    fn decrypt(&mut self, dst: &mut [u8], src: &[u8]) {
        unsafe {
            nettle_aes128_decrypt(
                &mut self.context as *mut _,
                min(src.len(), dst.len()),
                dst.as_mut_ptr(),
                src.as_ptr(),
            )
        };
    }

    fn context(&mut self) -> *mut c_void {
        ((&mut self.context) as *mut aes128_ctx) as *mut c_void
    }

    fn raw_encrypt_function() -> RawCipherFunctionPointer {
        RawCipherFunctionPointer::new(nettle_aes128_encrypt)
    }

    fn raw_decrypt_function() -> RawCipherFunctionPointer {
        RawCipherFunctionPointer::new(nettle_aes128_decrypt)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn set_key() {
        let key = &(b"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x10\x11\x12\x13\x14\x15\x16"[..]);
        let _ = Aes128::with_encrypt_key(key).unwrap();
        let _ = Aes128::with_decrypt_key(key).unwrap();
    }

    #[test]
    fn round_trip() {
        let key = vec![
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11,
            0x12, 0x13, 0x14, 0x15, 0x16,
        ];
        let input = vec![
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11,
            0x12, 0x13, 0x14, 0x15, 0x16,
        ];
        let mut cipher = vec![0; 16];
        let mut output = vec![0; 16];

        let mut enc = Aes128::with_encrypt_key(&key).unwrap();
        let mut dec = Aes128::with_decrypt_key(&key).unwrap();

        enc.encrypt(&mut cipher, &input);
        dec.decrypt(&mut output, &cipher);

        assert_eq!(output, input);
    }

    #[test]
    fn round_trip_invert() {
        let key = vec![
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11,
            0x12, 0x13, 0x14, 0x15, 0x16,
        ];
        let input = vec![
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11,
            0x12, 0x13, 0x14, 0x15, 0x16,
        ];
        let mut cipher = vec![0; 16];
        let mut output = vec![0; 16];

        let mut enc = Aes128::with_encrypt_key(&key).unwrap();
        let mut dec = Aes128::with_inverted_key(&enc);

        enc.encrypt(&mut cipher, &input);
        dec.decrypt(&mut output, &cipher);

        assert_eq!(output, input);
    }
}
